package mongoApp.documents;

import javax.annotation.Generated;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Users
 */
@Document
public class Users {

  @Id
  @Generated(value = "id")
  private String id;

  private String nama;

  private String perusahaan;

  private int penghasilan;

  @DBRef
  private Address address;

  @DBRef
  private School school;

  public Users() {
  }

  public Users(String id, String nama, String perusahaan, int penghasilan, Address address, School school) {
    this.id = id;
    this.nama = nama;
    this.perusahaan = perusahaan;
    this.penghasilan = penghasilan;
    this.address = address;
    this.school = school;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getNama() {
    return nama;
  }

  public void setNama(String nama) {
    this.nama = nama;
  }

  public String getPerusahaan() {
    return perusahaan;
  }

  public void setPerusahaan(String perusahaan) {
    this.perusahaan = perusahaan;
  }

  public int getPenghasilan() {
    return penghasilan;
  }

  public void setPenghasilan(int penghasilan) {
    this.penghasilan = penghasilan;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public School getSchool() {
    return school;
  }

  public void setSchool(School school) {
    this.school = school;
  }

}