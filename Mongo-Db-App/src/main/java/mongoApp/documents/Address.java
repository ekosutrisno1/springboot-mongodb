package mongoApp.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Address
 */
@Document(collection = "address")
public class Address {

  @Id
  private String id;
  private String alamatRumah;
  private String kodePost;

  public String getAlamatRumah() {
    return alamatRumah;
  }

  public void setAlamatRumah(String alamatRumah) {
    this.alamatRumah = alamatRumah;
  }

  public String getKodePost() {
    return kodePost;
  }

  public void setKodePost(String kodePost) {
    this.kodePost = kodePost;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Address() {
  }

  public Address(String id, String alamatRumah, String kodePost) {
    this.id = id;
    this.alamatRumah = alamatRumah;
    this.kodePost = kodePost;
  }

}