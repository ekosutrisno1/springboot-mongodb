package mongoApp.documents;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * School
 */
@Document(collection = "school")
public class School {
  private String pendidikanTerakhir;
  private String tahunLulus;

  public String getPendidikanTerakhir() {
    return pendidikanTerakhir;
  }

  public void setPendidikanTerakhir(String pendidikanTerakhir) {
    this.pendidikanTerakhir = pendidikanTerakhir;
  }

  public String getTahunLulus() {
    return tahunLulus;
  }

  public void setTahunLulus(String tahunLulus) {
    this.tahunLulus = tahunLulus;
  }

}