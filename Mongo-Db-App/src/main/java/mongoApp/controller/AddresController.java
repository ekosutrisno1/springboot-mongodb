package mongoApp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mongoApp.documents.Address;
import mongoApp.repositroy.AddressRepositrory;

/**
 * AddresController
 */
@RestController
@RequestMapping("rest/address")
public class AddresController {

  @Autowired
  private AddressRepositrory addressRepository;

  @GetMapping
  public List<Address> getAll() {
    return addressRepository.findAll();
  }

  @GetMapping("/{id}")
  public ResponseEntity<?> getById(@PathVariable("id") String id) {

    Optional<Address> aOptional = addressRepository.findById(id);
    if (aOptional.isPresent())
      return new ResponseEntity<>(aOptional.get(), HttpStatus.OK);

    return new ResponseEntity<>("Data-tidak-ditemukan-ID:" + id, HttpStatus.NOT_FOUND);
  }

  @PostMapping
  public Address insertUser(@RequestBody Address address) {
    return addressRepository.save(address);
  }

  @PutMapping("/{id}")
  public Address updateUser(@PathVariable("id") String id, @RequestBody Address address) {
    Address addressData = addressRepository.findById(id).get();

    addressData.setAlamatRumah(address.getAlamatRumah());
    addressData.setKodePost(address.getKodePost());

    return addressRepository.save(addressData);
  }

  @DeleteMapping("/{id}")
  public void deleteUsers(@PathVariable("id") String id) {
    addressRepository.deleteById(id);
  }

  @GetMapping("/page")
  public Page<Address> getAllPage(@PageableDefault(size = 2, sort = "id") Pageable pageable) {
    return addressRepository.findAll(pageable);
  }
}