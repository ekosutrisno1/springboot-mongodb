package mongoApp.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mongoApp.documents.Users;
import mongoApp.repositroy.UserRepository;

@RestController
@RequestMapping("/rest/allData")
public class UserController {

  static Logger logger = LoggerFactory.getLogger(UserRepository.class);

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private MongoTemplate mongoTemplate;

  @GetMapping
  public List<Users> getAll() {
    return userRepository.findAll();
  }

  @GetMapping("/page")
  public Page<Users> getAllPage(@PageableDefault(size = 5, sort = "id") Pageable pageable) {
    return userRepository.findAll(pageable);
  }

  @GetMapping("/search")
  public Page<Users> getByNamaUSers(@RequestParam("nama") String nama,
      @PageableDefault(size = 5, sort = "id") Pageable pageable) {
    return userRepository.findByNamaLikeIgnoreCase(nama, pageable);
  }

  @GetMapping("/{id}")
  public ResponseEntity<?> getById(@PathVariable("id") String id) {

    Optional<Users> dataUSer = userRepository.findById(id);
    if (dataUSer.isPresent())
      return new ResponseEntity<>(dataUSer.get(), HttpStatus.OK);

    return new ResponseEntity<>("Data-tidak-ditemukan-ID:" + id, HttpStatus.NOT_FOUND);
  }

  @PostMapping
  public Users insertUser(@RequestBody Users users) {
    return userRepository.save(users);
  }

  @PutMapping("/{id}")
  public Users updateUser(@PathVariable("id") String id, @RequestBody Users users) {
    Users dataUser = userRepository.findById(id).get();

    dataUser.setNama(users.getNama());
    dataUser.setPerusahaan(users.getPerusahaan());
    dataUser.setPenghasilan(users.getPenghasilan());

    return userRepository.save(dataUser);
  }

  @DeleteMapping("/{id}")
  public void deleteUsers(@PathVariable("id") String id) {
    userRepository.deleteById(id);
  }

  @GetMapping("/count")
  public List<Users> jumlahDataDenganKriteria(@RequestParam("gaji") int gaji) {
    List<Users> user = null;

    Query query = new Query();
    Criteria criteria = new Criteria();

    criteria = criteria.and("penghasilan").gte(gaji);
    query.addCriteria(criteria);

    user = this.mongoTemplate.find(query, Users.class);
    return user;
  }

  @GetMapping("/agregat")
  public List<Users> getAllAgregat() {
    return null;
  }
}