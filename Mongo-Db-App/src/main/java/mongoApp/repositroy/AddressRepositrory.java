package mongoApp.repositroy;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import mongoApp.documents.Address;

/**
 * AddressRepositrory
 */
@Repository
public interface AddressRepositrory extends MongoRepository<Address, String> {

}