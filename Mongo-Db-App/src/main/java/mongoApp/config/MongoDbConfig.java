package mongoApp.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MongoDbConfig {

  @Bean
  CommandLineRunner commandLineRunner() {
    return new CommandLineRunner() {

      @Override
      public void run(String... args) throws Exception {

      }
    };
  }

}