package Mongo.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import Mongo.model.Role;
import Mongo.repository.RoleRepository;

/**
 * Initial
 */
@Configuration
public class Initial {

  @Bean
  CommandLineRunner commandLineRunner(RoleRepository roleRepository) {
    return new CommandLineRunner() {

      @Override
      public void run(String... args) throws Exception {
        Role role = roleRepository.findByRole("ADMIN");
        if (role == null) {
          Role newRole = new Role();
          newRole.setRole("ADMIN");
          roleRepository.save(newRole);
        }
      }
    };
  }
}