package Mongo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Mongo.model.Product;
import Mongo.repository.ProductRepository;

/**
 * ProductController
 */
@RestController
@RequestMapping("/api/products")
public class ProductController {

  @Autowired
  private ProductRepository productRepository;

  @GetMapping
  public Iterable<Product> getAllProduct() {
    return productRepository.findAll();
  }

  @PostMapping
  public Product saveProduct(@RequestBody Product product) {
    return productRepository.save(product);
  }

  @GetMapping("/{id}")
  public ResponseEntity<?> getProductById(@PathVariable("id") String id) {
    Optional<Product> optionalProd = productRepository.findById(id);
    if (optionalProd.isPresent())
      return new ResponseEntity<>(optionalProd.get(), HttpStatus.OK);
    return new ResponseEntity<>("Data-tidak-ada-ID:" + id, HttpStatus.OK);
  }

  @PutMapping("/{id}")
  public Product update(@PathVariable("id") String id, @RequestBody Product product) {
    Product detProd = productRepository.findById(id).get();
    if (product.getProdName() != null)
      detProd.setProdName(product.getProdName());
    if (product.getProdDesc() != null)
      detProd.setProdDesc(product.getProdDesc());
    if (product.getProdPrice() != null)
      detProd.setProdPrice(product.getProdPrice());
    if (product.getProdImage() != null)
      detProd.setProdImage(product.getProdImage());

    return productRepository.save(detProd);
  }

  @DeleteMapping("/{id}")
  public String delete(@PathVariable("id") String id) {

    Optional<Product> product = productRepository.findById(id);

    if (!product.isPresent()) {
      return "Product dengan ID " + id + " tidak ditemukan!";
    }

    productRepository.delete(product.get());
    return "Product dengan ID " + id + " Berhasil Dihapus.";

  }

}