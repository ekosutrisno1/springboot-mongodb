package Mongo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Mongo.dto.AuthenData;
import Mongo.model.User;
import Mongo.repository.UserRepository;
import Mongo.security.JwtTokenProvider;
import Mongo.service.CustomUserDetailService;

/**
 * AuthController
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController {

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private JwtTokenProvider jwtTokenProvider;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CustomUserDetailService userDetailService;

  @SuppressWarnings("rawtypes")
  @PostMapping("/login")
  public ResponseEntity login(@RequestBody AuthenData data) {
    try {
      String username = data.getEmail();
      String password = data.getPassword();

      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

      String token = jwtTokenProvider.createToken(username, userRepository.findByEmail(username).getRoles());

      Map<Object, Object> model = new HashMap<>();
      model.put("username", username);
      model.put("token", token);
      return new ResponseEntity<>(model, HttpStatus.OK);

    } catch (AuthenticationException e) {
      throw new BadCredentialsException("Password / Username tidak Valid");
    }
  }

  @SuppressWarnings("rawtypes")
  @PostMapping("/register")
  public ResponseEntity register(@RequestBody User user) {
    User existUser = userDetailService.findUserByEmail(user.getEmail());
    if (existUser != null)
      throw new BadCredentialsException("User " + user.getEmail() + " Telah digunakan!");

    userDetailService.addUser(user);
    Map<Object, Object> model = new HashMap<>();
    model.put("message", "User dengan nama " + user.getEmail() + " telah terdaftar.");

    return new ResponseEntity<>(model, HttpStatus.OK);
  }

}