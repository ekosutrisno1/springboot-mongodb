package Mongo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import Mongo.model.Product;

/**
 * ProductRepository
 */
@Repository
public interface ProductRepository extends CrudRepository<Product, String> {

}