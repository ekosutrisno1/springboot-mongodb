package Mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import Mongo.model.User;

/**
 * UserRepositroy
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {

  User findByEmail(String email);

}