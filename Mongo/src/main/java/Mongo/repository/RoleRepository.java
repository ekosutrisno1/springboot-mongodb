package Mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import Mongo.model.Role;

/**
 * RoleRepositroy
 */
@Repository
public interface RoleRepository extends MongoRepository<Role, String> {

  Role findByRole(String role);

}